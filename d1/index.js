//SECTION - Exponent operator


//pre-es6
/*
	syntax:
	let/const variableName = Math.pow(number,exponent)
*/
let firstNum = Math.pow(8,2);
console.log(firstNum);

//es6
/*
	syntax:
	let/const variableName = number ** exponent;
*/
let secondNum = 8**2;
console.log(secondNum);









//SECTION - template literals
/*
	-allows strings without using the concatination operator(+);
	-helps greatly in terms of code readability;
*/

let name = "John";

//pre-es6 

let message = "Hello " + name + " !Welcome to programming! ";

//message without template literals: message
console.log("Message without template literals: " + message);

//es6
//single-line
message = `Hello ${name}! Welcome to programming`;
console.log(`Message without template literals: " ${message}`);


//multiple-line
const anotherMessage = `
${name} won the math competition.
He won it by solving the problem 8**2 with the solution of ${8**2}.
`;
//He won it by solving the problem 8**2 with the solution of ${secondNum}.

console.log(anotherMessage);


//mini-activity

let interestRate = .15;
let principal = 1000;

console.log(`Total interest: ${principal*interestRate} of the account that has ${principal} as its balance and the ${interestRate} as its interest %`);








//SECTION - Array Destructuring
/*
	-allows us to unpack elements in arrays to distinct variables
	-allows us to name array elements with variables instead of using index numbers
	syntax:
		let/const [variableA, variableB, variableC] = arrayName;
*/

const fullName = ['Juan',"Dela","Cruz"];

//pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

//array destructuring
const [ firstName, middleName, lastName ] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

//using template literals
console.log(`Hello ${firstName} ${middleName} ${lastName}!`);



//SECTION - Object destructuring
/*
		Syntax:
		let/const {propertyA,propertyB,propertyC} = objectName;
*/
//mini-activity
const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz',
}

//pre-es6 / pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}!`);



//object destructuring
const { givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);

//using object destructuring as a parameter of a function
function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
};

getFullName(person);


//mini-activity
const pet = {
	name: 'polgoso',
	trick: 'somersault',
	treat: 'carrot',
}


function getPetName({name, trick, treat}){
	console.log(`${name}`);
	console.log(`good ${trick}!`);
	console.log(`here is ${treat} for you!`);
}
getPetName(pet);





//SECTION Arrow Function
/*
	-compact alternative syntax to traditional functions
*/
/*const hello = ()=>{
	console.log("Hello World");
};*/

//pre es6/arrow function
/*function printFullName(firstName,middleInitial,lastName){
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}*/

//arrow function in es6
/*
		Syntax:
		let/const variableName = (parameterA,parameterB,parameterC) =>{
			console.log(); /statement/expressions
		}
*/
let printFullName = (firstName,middleInitial,lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}
printFullName("Portgas", "D.", "Ace");




//SECTION - Implicit return statement
/*
	-There are instances where we can omit the "return statement;
	-this works because even without the "return" statement, JS can implicitly adds it for the result of the function
*/
//arrow function with loops
//pre-arrow function

const students = ["John","Jane","Judy"];
students.forEach(function(student){
	console.log(`${student} is a student`);
});

//mini-activity

/*let add = (x,y) => {
	return x+y;
}
console.log(add(5,3));*/



const add = (x,y) => x+y;
let addVariable = add(99,888);
console.log(addVariable);


const greet = (name = "User", age = "Unknown") => {
	return `Good morning ${name} ${age}!` 
}

console.log(greet("John", 15));


//SECTION - Class-based Object Blueprints





/*
		Creating a class
		-"constructor" is a special method of a class for creating/initializing an object for that class
		-"this" refers to the properties of an object created from the class; this allow us to reassign values for the properties inside the class.
		-"new" creates a new object with the gvien arguments as the values of its properties

		Syntax:
		class ClassName{
			constructor (objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
				this.objectPropertyC = objectPropertyC;
			}
		}
		let/const variableName = new ClassName();

*/






class Car{
	constructor (brand,name,year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};

const myCar = new Car("Honda","Brio", 2022);

//1st instance/instatiation of objects
//using initializer and/or dot notation, create a car object using CAr class that we have created.
//Value for each property under the car class
myCar.brand = "Honda"
myCar.name = "Range Raptor"
myCar.year = 2021;
console.log(myCar);

//2nd instance - using arguments

const myNewCar = new Car("Toyota","Vios",2021);
console.log(myNewCar);